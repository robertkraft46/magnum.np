1.1.6 [2024-04-11]
- added biquadratic RKKY interaction

1.1.5 [2024-02-22]
- added Slonczewski Spin Transfer Torque
- fixed sign in DMI when using PBCs

1.1.4 [2023-11-08]
- added Dispersion Calculator demo
- added dispersion to eigenmode solver

1.1.3 [2023-10-09]
- added ThermalField
- added Langevin demo

1.1.2 [2023-07-12]
- added MinimizerBB
- use demos as integration tests

1.1.1 [2023-06-13]
- added eigensolver
- added RUX
- fixed float32 bug in demag field

1.1.0 [2023-05-24]
- added pseudo PBCs
- some fixes for non-equidistant code

1.0.9 [2023-05-03]
- added nonequidistant mesh support
- refactor demag field
- added TorchDiffEqAdjoint solver
- improved documentation

1.0.8 [2023-02-21]
- fixed bug in exchange field 
- improved imaging tools

1.0.7 [2023-02-21]
- hot-fixed missing package in setup.py

1.0.6 [2023-02-20]
- use pytorch.compile if available
- material setter converts arbitrary input to tensor-fields

1.0.5 [2023-02-13]
- move documentation to gitlab pages
- refactor ODE solver interface
- wrappers for scipy ODE solvers

1.0.4 [2023-01-30]
- improved demos and unit tests

1.0.3 [2023-01-23]
- test deploy process

1.0.2 [2023-01-19]
- update PYPI project informations

1.0.1 [2023-01-19]
- fix gitlab.ci pipeline

1.0.0 [2023-01-19]
- first public version

0.9.9 [2023-01-16]
- initial commit
